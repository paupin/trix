package trix

import (
	"testing"
)

func TestInternalSet(t *testing.T) {
	n := internalSet(nil, nil, nil)
	testTrue(t, n == nil)
}
