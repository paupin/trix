package trix

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
)

func testEqualString(t *testing.T, actual, expected interface{}) {
	t.Helper()
	if fmt.Sprint(actual) != fmt.Sprint(expected) {
		t.Errorf(`Expected "%v", got "%v"`, expected, actual)
	}
}

func testDeepEqual(t *testing.T, actual, expected interface{}) {
	t.Helper()
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf(`Expected "%v" (%T), got "%v" (%T)`, expected, expected, actual, actual)
	}
}

func testError(t *testing.T, err error, expected string) {
	t.Helper()
	if expected == "" {
		if err != nil {
			t.Errorf(`Expected not error, got "%v"`, err)
		}

	} else if err == nil {
		t.Errorf(`Expected error "%s", got none`, expected)
	} else if actual := err.Error(); actual != expected {
		t.Errorf(`Expected error "%s", got "%s"`, expected, actual)
	}
}

func testTrue(t *testing.T, value bool) {
	t.Helper()
	if !value {
		t.Errorf(`Expected true, got "%v"`, value)
	}
}

func testJSON(t *testing.T, v interface{}, expectedJSON string) {
	t.Helper()
	buf, err := json.Marshal(v)
	if err != nil {
		t.Errorf("encoding error")
	} else if actualJSON := string(buf); actualJSON != expectedJSON {
		t.Errorf("Expected\n%s\nbut instead got:\n%s", expectedJSON, actualJSON)
	}
}
