package trix

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"sort"
	"strconv"
)

// StrArgs is a string-string map
type StrArgs map[string]string

// NumericStringSlice represents a string slice that can be sorted using the
// integer representation of its values
type NumericStringSlice []string

// Sort this slice.
func (s NumericStringSlice) Sort()         { sort.Sort(s) }
func (s NumericStringSlice) Len() int      { return len(s) }
func (s NumericStringSlice) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s NumericStringSlice) Less(i, j int) bool {
	ii, _ := strconv.Atoi(s[i])
	ij, _ := strconv.Atoi(s[j])
	return ii < ij
}

// Args represents a generic string-interface{} map
type Args map[string]interface{}

// set a value decoded from JSON, converting numbers as appropriate
func (args Args) set(key string, value interface{}) {
	switch value.(type) {
	case json.Number:
		// parse JSON numbers, first as ints then as floats
		s := string(value.(json.Number))
		if i32, err := strconv.ParseInt(s, 10, 32); err == nil {
			value = int(i32)
		} else if i64, err := strconv.ParseInt(s, 10, 64); err == nil {
			value = i64
		} else if f64, err := strconv.ParseFloat(s, 64); err == nil {
			value = f64
		} else {
			value = s
		}
	}
	args[key] = value
}

// Merge the args with a map, slice or array, adding or overwriting keys
func (args Args) Merge(other interface{}) Args {
	if other == nil {
		// noop

	} else if a, isArgs := other.(Args); isArgs {
		// regular Args, merge it normally
		for key, value := range a {
			args.set(key, value)
		}

	} else if rkind := reflect.TypeOf(other).Kind(); rkind == reflect.Map {
		// generic map, cast keys to strings and set values
		rval := reflect.ValueOf(other)
		for _, key := range rval.MapKeys() {
			val := rval.MapIndex(key)
			args.set(fmt.Sprint(key), val.Interface())
		}

	} else if rkind == reflect.Array || rkind == reflect.Slice {
		// array/slice: use 1-based numeric keys, that aren't yet used,
		// so we never overwrite, only append.
		rval := reflect.ValueOf(other)
		index := 1
		for i, l := 0, rval.Len(); i < l; i++ {
			// find an unused key for the current item
			var key string
			for {
				key = fmt.Sprint(index)
				if _, found := args[key]; !found {
					break
				}
				index++
			}
			args.set(key, rval.Index(i).Interface())
		}
	}

	return args
}

// MergeJSON will decode the JSON buffer into a new set of Args, and merge it
func (args Args) MergeJSON(data []byte) Args {
	var other Args
	dec := json.NewDecoder(bytes.NewBuffer(data))
	dec.UseNumber()
	if err := dec.Decode(&other); err != nil {
		return args
	}
	return args.Merge(other)
}

// Clone returns a clone of the original one
func (args Args) Clone() Args {
	n := Args{}
	for k, v := range args {
		n[k] = v
	}
	return n
}

// Add returns a new map, adding or overwriting keys
func (args Args) Add(other Args) Args {
	return args.Clone().Merge(other)
}

// GetStringDefault returns the specified key as a string,
// or the default if the key is not found
func (args Args) GetStringDefault(key, def string) string {
	if v, found := args[key]; found {
		if s, ok := v.(string); ok {
			return s
		}
		return fmt.Sprint(v)
	}
	return def
}

// GetString returns the specified key as a string
func (args Args) GetString(key string) string {
	return args.GetStringDefault(key, "")
}

// String returns a simple string representation of the arguments, with the
// keys sorted. This is mainly convenient for testing.
func (args Args) String() string {
	// get sorted list of keys
	keys := make([]string, 0, len(args))
	for key := range args {
		keys = append(keys, key)
	}
	sort.StringSlice(keys).Sort()

	// write keys/values in a format similar to `fmt.printValue`
	buf := bytes.Buffer{}
	buf.WriteString("args[")
	first := true
	for _, key := range keys {
		if !first {
			buf.WriteString(" ")
		}
		first = false
		fmt.Fprintf(&buf, `%s:%v`, key, args[key])
	}
	buf.WriteString("]")
	return buf.String()
}

// Equals returns whether this set of args matches another.
// That is, if they both have the same keys and values.
func (args Args) Equals(other Args) bool {
	if len(args) != len(other) {
		return false
	}
	for key, expected := range args {
		actual, found := other[key]
		if !found {
			return false
		}
		if actual != expected {
			return false
		}
	}
	return true
}

// Fill adds values to a key. If that key is already of type []interface{},
// each new value gets appended to the slide. Otherwise, a new slice of that
// type is created, and existing values are copied into it before the new ones.
func (args Args) Fill(key string, values ...interface{}) {
	var arr []interface{}

	if a, found := args[key]; !found {
		// new value
		arr = []interface{}{}

	} else if _, ok := a.([]interface{}); ok {
		// it's already the right kind of array
		arr = a.([]interface{})

	} else if k := reflect.ValueOf(a).Kind(); k == reflect.Array || k == reflect.Slice {
		// other arrays: add existing values to new array
		arr = []interface{}{}
		rval := reflect.ValueOf(a)
		for i, l := 0, rval.Len(); i < l; i++ {
			// find an unused key for the current item
			arr = append(arr, rval.Index(i).Interface())
		}

	} else {
		// add element to a new array
		arr = []interface{}{a}
	}

	for _, value := range values {
		arr = append(arr, value)
	}
	args[key] = arr
}
