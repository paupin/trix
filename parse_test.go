package trix

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math"
	"testing"
	"time"
)

func TestParseBool(t *testing.T) {
	ck := func(v interface{}, expected bool, expectedError string) {
		t.Helper()
		actual, err := ParseBool(v)
		testError(t, err, expectedError)
		testDeepEqual(t, actual, expected)
	}

	ck("1", true, "")
	ck(1, true, "")
	ck("t", true, "")
	ck("T", true, "")
	ck("true", true, "")
	ck("TRUE", true, "")
	ck(true, true, "")
	ck("on", true, "")
	ck("ON", true, "")
	ck("0", false, "")
	ck(0, false, "")
	ck("f", false, "")
	ck("F", false, "")
	ck("false", false, "")
	ck("FALSE", false, "")
	ck("off", false, "")
	ck("OFF", false, "")

	ck(nil, false, "bad value")
	ck("", false, "bad value")
	ck("untrue", false, "bad value")
}

func TestParseInt(t *testing.T) {
	ck := func(v interface{}, expected int, expectedError string) {
		t.Helper()
		actual, err := ParseInt(v)
		testError(t, err, expectedError)
		testDeepEqual(t, actual, expected)
	}

	// from strconv/atoi_test.go
	ck("", 0, `strconv.ParseInt: parsing "": invalid syntax`)
	ck("0", 0, "")
	ck("-0", 0, "")
	ck("1", 1, "")
	ck("-1", -1, "")
	ck("12345", 12345, "")
	ck("-12345", -12345, "")
	ck("012345", 12345, "")
	ck("-012345", -12345, "")
	ck("98765432100", 98765432100, "")
	ck("-98765432100", -98765432100, "")
	ck("9223372036854775807", 1<<63-1, "")
	ck("-9223372036854775807", -(1<<63 - 1), "")
	ck("9223372036854775808", 1<<63-1, `strconv.ParseInt: parsing "9223372036854775808": value out of range`)
	ck("-9223372036854775808", -1<<63, "")
	ck("9223372036854775809", 1<<63-1, `strconv.ParseInt: parsing "9223372036854775809": value out of range`)
	ck("-9223372036854775809", -1<<63, `strconv.ParseInt: parsing "-9223372036854775809": value out of range`)

	ck(nil, 0, `strconv.ParseInt: parsing "<nil>": invalid syntax`)
	ck(0, 0, "")
	ck(999, 999, "")
	ck(math.Pi, 0, `strconv.ParseInt: parsing "3.141592653589793": invalid syntax`)
}

func TestInternalMergeFile(t *testing.T) {
	testError(t,
		NewNode("").GraftFile("missing-file"),
		"open missing-file: no such file or directory",
	)

	testError(t,
		NewNode("").GraftFile("tests/missing-test.conf"),
		`tests/missing-test.conf:1: including "tests/missing-file.conf": open tests/missing-file.conf: no such file or directory`,
	)

	node := NewNode("")
	testError(t, node.GraftFile("tests/include-simple.conf"), "")
	testJSON(t, node, `{"a":"3","b":{"c":"3"}}`)

	root := NewRoot()
	testError(t, root.GraftFile("tests/types.conf"), "")
	ck := func(key, expectedType string, expected Value) {
		t.Helper()
		v := root.Get(key)
		testEqualString(t, fmt.Sprintf("%T", v), expectedType)
		testDeepEqual(t, v, expected)
	}

	expectedTime, _ := time.Parse("2006-01-02 15:04:05", "1979-12-07 00:00:00")
	ck("v.s", "string", "a")
	ck("v.i", "int", 1)
	ck("v.f", "float64", 3.14)
	ck("v.b", "bool", true)
	ck("v.d", "time.Duration", time.Hour)
	ck("v.t", "time.Time", expectedTime)
	ck("a.s", "[]string", []string{"a", "b", "c"})
	ck("a.i", "[]int", []int{1, 2, 3})
	ck("a.f", "[]float64", []float64{3.14, 3.15, 3.16})
	ck("a.b", "[]bool", []bool{
		true, true, true, true,
		false, false, false, false,
	})
	ck("a.d", "[]time.Duration", []time.Duration{
		time.Hour, time.Minute,
		3 * 24 * time.Hour,
		time.Hour + 2*time.Minute + 3*time.Second,
	})

	ck("a.t", "[]time.Time", []time.Time{
		expectedTime, expectedTime, expectedTime, expectedTime, expectedTime,
		expectedTime, expectedTime, expectedTime, expectedTime, expectedTime,
		expectedTime, expectedTime,
	})

	testError(t,
		node.GraftReader(bytes.NewBufferString(`bad syntax`), true),
		`line 1: bad format: "bad syntax"`,
	)

	node.GraftReader(bytes.NewBufferString(`
		a=8
		b.d=4
	`), false)
	testJSON(t, node, `{"a":"8","b":{"c":"3","d":"4"}}`)
}

func TestParseJSON(t *testing.T) {
	data := []byte(`
		{"a":1,"b":"lolcats","c":{"d":3.1415},"d":[1,2,3],"e":[1,"two",3.0,true]}
	`)

	node := NewRoot()
	err := json.Unmarshal(data, node)
	testError(t, err, "")
	node.SortRecursively()
	testJSON(t, node, `{"a":1,"b":"lolcats","c":{"d":3.1415},"d":[1,2,3],"e":[1,"two",3,true]}`)
	testDeepEqual(t, node.Get("c.d"), 3.1415)
	testDeepEqual(t, node.Get("e.4"), true)
}
